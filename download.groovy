@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.3')
import static groovyx.net.http.HttpBuilder.configure
import groovyx.net.http.optional.Download

File root = new File('boes')
root.mkdirs()

String pattern = 'BOE-A-%04d-%d.pdf'
int year = args[0] as int
String urlPattern = 'https://www.boe.es/boe/dias/%04d/%02d/%02d/pdfs/%s'

int month=1
int day=1
int yearTope = new Date()[Calendar.YEAR]
int monthTope = new Date()[Calendar.MONTH]+1
int counter = 1

if( args.length > 1){
    yearTope = args[1] as int
    monthTope = 12
}

while( year < yearTope || month < monthTope){
    println "$year/$month/$day $counter"
    String boeName = String.format(pattern,year,counter)
    String url = String.format(urlPattern,year,month,day,boeName)
    File file = new File(root,boeName)
    if( file.exists() == false){
        try{
            File boe = configure {
                request.uri = url
            }.get {
                println url
                Download.toFile(delegate, file)
            }
        }catch(e){
            println "-".multiply(10)
            file.delete()
            counter=0
            day++
            if(day > 31){            
                day=1            
                month++
                if(month>12){
                    month=1
                    year++
                }
            }
        }
    }
    counter++    
}
