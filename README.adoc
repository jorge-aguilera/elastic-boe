= Elastic BOE
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:toc: left

Buscador de texto en los Boletines Oficiales del Estado (BOE)

(repositorio alojado en https://gitlab.com/jorge-aguilera/elastic-boe)

== Objetivo

Disponer de un buscador personal sobre contenidos parseados de los BOEs.

== Arquitectura

=== Elasticsearch

Servicio que parsea e indexa contenido de diferentes tipos

=== Kibana

Aplicación Web que sirve de interface para realizar querys a Elasticsearch
y mostrar los resultados (entre otros)

=== fsCrawler

Aplicación que periódicamente escanea un directorio en busca de ficheros nuevos
y los envía a Elasticsearch para su indexación

=== Download.groovy

Script groovy que descarga todos los BOEs desde un año dado hasta la fecha. Si el 
fichero ya se encuentra descargado lo ignora y continúa con el siguiente

== Requisitos

Como sistema operativo se debería usar Linux (no se ha probado en otros sistemas)

Así mismo debemos tener instalada una versión actualizada de Docker y Docker Compose

== Instalación

Descargar o clonar el proyecto desde https://gitlab.com/jorge-aguilera/elastic-boe

- Crear el servicio Elasticsearch y comprobar en los logs que se inicia normalmente:

[source,console]
----
$ docker-compose up -d elasticsearch
$ docker-compose logs -f
----

- Crear el servicio Kibana y comprobar en los logs que se inicia normalmente:

[source,console]
----
$ docker-compose up -d kibana
$ docker-compose logs -f
----

- Crear el servicio fsCrawler y comprobar en los logs que se inicia normalmente:

[source,console]
----
$ docker-compose up -d fscrawler
$ docker-compose logs -f
----

Si todo ha ido bien hasta este punto únicamente tenemos una seria de servicios conectados
entre sí pero sin datos. 

== Descarga de BOEs

Para comprobar que el sistema funciona correctamente descargaremos manualmente un BOE y lo
copiaremos en el subdirectorio _boes_

Desde un navegador abriremos http://localhost:5601 y crearemos un indice de Kibana usando
el que nos ofrece _boe_ *sin* usar un atributo de tiempo

Si todo ha ido bien deberíamos ser capaces de ver el BOE que hemos ubicado en _boes_

Para la descarga masiva de BOES utilizaremos el sh _download.sh_ proporcionando un año de inicio
como primer argumento y un año de fin opcional

[source,console]
----
$./download.sh 2015 2017
----

Este proceso empezará a descargar ordenadamente los documentos siguiendo el patrón que utiliza
la web para alojarlos : https://www.boe.es/boe/dias/YYYY/MM/DD/pdfs/BOE-A-YYYY-CONTADOR.pdf

Según vayan siendo detectados por fscrawler este los irá enviando a Elasticsearch para su indexación

En este vídeo se puede el funcionamiento (audio defectuoso)

video::Pikda2vvwG4[youtube]


